# SETUP #

Steps for running docker-compose in your project


### Clone set-up repo to your local project ###

### Build image for frontend ###

* Go to your frontend folder and run this commnad in terminal:  

		docker build -t frontend .

### Build image for backend ###

* Go to your backend folder and run this command in terminal:  

		docker build -t backend .
		
### Create a network for docker-compose ###
		docker network create app_net
		
###  Run docker-compose.yml 
* Make sure you have postgress password, user, db from your localhost in docker-compose.yml ( POSTGRES_DB, etc..)
* Go to your docker-compose.yml folder and run this commnad: 		
		
		docker-compose up
		
### You're ready to code!
